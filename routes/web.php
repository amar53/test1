<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/bank', 'BankController@index')->name('bank');
Route::get('/input_data', 'BankController@inputDataBank')->name('input_data');
Route::post('/postdataBank', 'BankController@store')->name('postdataBank');

Route::get('/edit_data/{id}','BankController@edit')->name('edit_data');
Route::post('/update_data/{id}','BankController@update')->name('update_data');
Route::post('/hapus_data/{id}','BankController@destroy')->name('hapus_data');