
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Document</title>
</head>
<body>
    <h2>Data Table Bank</h2>

<div class="preload"><img src="http://i.stack.imgur.com/MnyxU.gif">
</div>

<div class="content">
    <div class="all">
        <div class="buttons">
            <div class="one">
            <a href="{{route('input_data')}}"><button class="square-shadow">Tambah Data</button></a>
            </div>
        </div>
    </div>
    
    <div class="table-wrapper">
        <table class="fl-table">
            <thead>
            <tr>
                <th>id</th>
                <th>Bank_name</th>
                <th>logo</th>
                <th>Contact_email</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
                @foreach ($bank as $value)
            <tr>
               <td>{{++ $i}}</td>
                <td>{{$value->bank_name}}</td>
                <td><img src="{{asset("assets/logo/$value->logo")}}" alt=""></td>
                <td>{{$value->contact_email}}</td>
                <th>  
                    <a type="button" name="show" data-id="{{$value->id}}" class="edit btn btn-info btn-sm Editpemasukan" href="{{route('edit_data',$value->id)}}">
                         <i class="fa fa-edit">Edit</i>
                    </a>
                <a type="submit" name="show" class="hapus"  data-id="{{$value->id}}" class="delete btn btn-danger btn-sm Deletepemasukan " href="#"> 
                        <i class="fa fa-trash-o">Hapus</i>
                    </a>
                </th>
            </tr>
            @endforeach
            <tbody>
        </table>
    </div>
</div>
</body>
</html>


<style>
.content {display:none;}
.preload { width:100px;
    height: 100px;
    position: fixed;
    top: 50%;
    left: 50%;}
    *{
    box-sizing: border-box;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
}
body{
    font-family: Helvetica;
    -webkit-font-smoothing: antialiased;
    background: rgba( 71, 147, 227, 1);
}
h2{
    text-align: center;
    font-size: 18px;
    text-transform: uppercase;
    letter-spacing: 1px;
    color: white;
    padding: 30px 0;
}

/* Table Styles */

.table-wrapper{
    margin: 10px 70px 70px;
    box-shadow: 0px 35px 50px rgba( 0, 0, 0, 0.2 );
}

.fl-table {
    border-radius: 5px;
    font-size: 12px;
    font-weight: normal;
    border: none;
    border-collapse: collapse;
    width: 100%;
    max-width: 100%;
    white-space: nowrap;
    background-color: white;
}

.fl-table td, .fl-table th {
    text-align: center;
    padding: 8px;
}

.fl-table td {
    border-right: 1px solid #f8f8f8;
    font-size: 12px;
}

.fl-table thead th {
    color: #ffffff;
    background: #4FC3A1;
}


.fl-table thead th:nth-child(odd) {
    color: #ffffff;
    background: #324960;
}

.fl-table tr:nth-child(even) {
    background: #F8F8F8;
}

/* Responsive */

@media (max-width: 767px) {
    .fl-table {
        display: block;
        width: 100%;
    }
    .table-wrapper:before{
        content: "Scroll horizontally >";
        display: block;
        text-align: right;
        font-size: 11px;
        color: white;
        padding: 0 0 10px;
    }
    .fl-table thead, .fl-table tbody, .fl-table thead th {
        display: block;
    }
    .fl-table thead th:last-child{
        border-bottom: none;
    }
    .fl-table thead {
        float: left;
    }
    .fl-table tbody {
        width: auto;
        position: relative;
        overflow-x: auto;
    }
    .fl-table td, .fl-table th {
        padding: 20px .625em .625em .625em;
        height: 60px;
        vertical-align: middle;
        box-sizing: border-box;
        overflow-x: hidden;
        overflow-y: auto;
        width: 120px;
        font-size: 13px;
        text-overflow: ellipsis;
    }
    .fl-table thead th {
        text-align: left;
        border-bottom: 1px solid #f7f7f9;
    }
    .fl-table tbody tr {
        display: table-cell;
    }
    .fl-table tbody tr:nth-child(odd) {
        background: none;
    }
    .fl-table tr:nth-child(even) {
        background: transparent;
    }
    .fl-table tr td:nth-child(odd) {
        background: #F8F8F8;
        border-right: 1px solid #E6E4E4;
    }
    .fl-table tr td:nth-child(even) {
        border-right: 1px solid #E6E4E4;
    }
    .fl-table tbody td {
        display: block;
        text-align: center;
    }
}

a {
    width: 100%;
    height: 100%;}

button {
  font-family: 'Muli', sans-serif;
  -webkit-transition: 0.15s all ease-in-out;
  -o-transition: 0.15s all ease-in-out;
  transition: 0.15s all ease-in-out;
  cursor: pointer; }

.all {
  padding: 30px 0;
  width: 90%;
  height: auto;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  margin-left: 5%;
  -webkit-box-pack: justify;
  -ms-flex-pack: justify;
  justify-content: space-between;
  -webkit-box-align: right;
  -ms-flex-align: right;
  align-items: right; }

.buttons {
  width: 80%;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-pack: justify;
  -ms-flex-pack: justify;
  justify-content: space-between;
  -webkit-box-align: right;
  -ms-flex-align: right;
  align-items: right; }

.one {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-pack: right;
  -ms-flex-pack: right;
  justify-content: right; }

.square {
  background: #0069eb;
  color: #fff;
  border: none;
  border-radius: 0px;
  font-size: 16px;
  padding: 15px 30px;
  text-decoration: none; }

.square-shadow {
  background: #0069eb;
  color: #fff;
  border: none;
  border-radius: 0px;
  font-size: 16px;
  padding: 15px 30px;
  text-decoration: none;
  -webkit-box-shadow: 4px 4px 10px rgba(0, 0, 0, 0.5);
  box-shadow: 4px 4px 10px rgba(0, 0, 0, 0.5); }

</style>



<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
@include('sweetalert::alert', ['cdn' => "https://cdn.jsdelivr.net/npm/sweetalert2@9"])

@include('sweetalert::alert')

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script>
    

   $('.hapus').on('click', function () {
            $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var id = $(this).attr('data-id');
        console.log(id);
        event.preventDefault();
        swal({
            title: "Are you sure?",
            text: "Jika anda yakin maka data ini akan di hapus!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                $.post("hapus_data/"+id, {
                   },
                  function(data) {
                    swal("Data berhasil terhapus", {
                                icon: "success",
                                button: false,
                            });
                });

                 
              setTimeout(function() {
                window.location.reload();
                
                }, 2000);
            } else {
                swal("Oke Data anda masih utuh !");
            }
        });
    });

    $(function() {
    $(".preload").fadeOut(2000, function() {
        $(".content").fadeIn(1000);        
    });
});

</script>