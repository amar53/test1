<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BankValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'bank_name'  => 'required|min:3',
            'logo' => 'required|image|mimes:jpg,png',
            'contact_email' => 'required|email|unique:bank',
            // 'contact_email' => 'required|email',
        ];
    }

    public function messages()
    {
        return [
            'bank_name.required.min' => 'Nama Bank Wajib Di isi & minimal 3 karakter ',
            'logo.mimes' => 'Logo harus Png atau Jpg',
            'contact_email.required.unique:bank' => 'Email Wajib Di isi Dan Unik ',
        ];
    }
}
