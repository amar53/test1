<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Bank;
use DB;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\BankValidation;
use  File;
use Intervention\Image\ImageManagerStatic as Image;
use RealRashid\SweetAlert\Facades\Alert;

class BankController extends Controller
{
    public function index(){
        $bank = Bank::orderBy('created_at', 'DESC')->paginate(10);
        return view('bank.index', compact('bank'))->with('i');
    }

    public function store(BankValidation $request)
    {

        $image = $request->file('logo');
        $name = $image->getClientOriginalName();
        $size = $image->getSize();
        $image_resize = Image::make($image->getRealPath());              
        $image_resize->resize(100, 100);
        $image_resize->save(public_path('/assets/logo/' .$name));
        // $destinationPath = public_path('/assets/logo/');
        // $image->move($destinationPath, $name); 

        $bank = Bank::create([
            'bank_name' => $request->bank_name,
            'logo' => $name,
            'contact_email' => $request->contact_email,
            ]);

        if ($bank == true) {
            Alert::success('Success ', 'Data Berhasil di simpan');
        }else{
            Alert::warning('Warning ', 'Data Gagal disimpan'); 
        }
        return redirect()->route('bank');
    }

    public function inputDataBank(){
        return view('bank.create');
    }

    public function edit($id)
    {
        $bank = Bank::find($id);
        return view('bank.edit', compact('bank'));
    }

    public function update(Request $request , $id)
    {

        if ($request->file('logo')) {
            $image = $request->file('logo');
            $name = $image->getClientOriginalName();
            $size = $image->getSize();
            $destinationPath = public_path('/assets/logo/');
            $image->move($destinationPath, $name);
            
            $bank = Bank::where('id',$id)->update([
                'bank_name' => $request->bank_name,
                'logo' => $name,
                'contact_email' => $request->contact_email,
            ]);
        }else{

        $bank = Bank::where('id',$id)->update([
            'bank_name' => $request->bank_name,
            'contact_email' => $request->contact_email,
        ]);
        }

        if ($bank == true) {
            Alert::success('Success ', 'Data Berhasil di update'); 
        }else{
            Alert::warning('Warning ', 'Data Gagal di update');
        }
        return redirect()->route('bank');
    }
    
    public function destroy($id)
    {
        $bank = Bank::where('id', $id)->first();
        $image_path = public_path('/assets/logo/' .$bank['logo']);
        if(File::exists($image_path)) {
            File::delete($image_path);
        }
        Bank::find($id)->delete();

        Alert::success('Success ', 'Data Berhasil di hapus'); 
        return redirect()->route('bank');
    }
}
